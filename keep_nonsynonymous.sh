
nonsynonymous_chrompos () {
#1 vcf input
#2 genome fasta 
#3 genome gff3
#4 prefix
#5 uniprot table
anvage synonymous --vcf $1 \
--genome $2 \
--annotation $3 \
--output_prefix $4

vcftools --positions <(awk '{print $1" "$2}' $5 | sort | uniq) --vcf $4"_nonsynonymous.vcf" --recode --out $4"_uniprot_nonsynonymous"
grep -v "^#" $4"_uniprot_nonsynonymous.recode.vcf" | awk '{ print $1"\t"$2}' | while read CHROMPOS; do grep "$CHROMPOS" $5; done > $4"_uniprot_nonsynonymous.tsv"

}

nonsynonymous_chrompos diplodus_CDS.vcf genomes/diplodus_genome_lgt6000.fasta annotation/sar_annotation.gff3 diplodus_CDS diplodus_CDS_uniprot_blast_filtered.tsv
nonsynonymous_chrompos mullus_CDS.vcf genomes/mullus_genome_lgt6000.fasta annotation/mullus_annotation.gff3 mullus_CDS mullus_CDS_uniprot_blast_filtered.tsv 

# to compare lists of SNP 

## Diplodus
# get CDS positions
grep -v "^##" "diplodus/diplodus_CDS.vcf" | cut -f1-2 | sed '1d' > "diplodus/diplodus_CDS_pos.txt"
# get pcadapt outlier positions
grep -v "^##" "../../seaConnect--dataPrep/04-finalPrep/01-Diplodus/dip_adaptive_413.vcf" | cut -f1-2 | sed '1d' > "diplodus/diplodus_pcadapt_pos.txt"

## Mullus
# get CDS positions
grep -v "^##" "mullus/mullus_CDS.vcf" | cut -f1-2 | sed '1d' > "mullus/mullus_CDS_pos.txt"
# get pcadapt outlier positions
grep -v "^##" "../../seaConnect--dataPrep/04-finalPrep/02-Mullus/mul_adaptive_291.vcf" | cut -f1-2 | sed '1d' > "mullus/mullus_pcadapt_pos.txt"
