## get flanking sequences centered on SNP genome coordinates
for i in `echo diplodus mullus`;
 do anvage flank -f ${i}_CDS.vcf -g genomes/${i}_genome_lgt6000.fasta -w 200 -o ${i}_CDS;
done
