colonne_rank_query () {
   uniprot_blastx_out=$1
   j="rien"
   count=0
   for i in `grep -ve "^#" ${uniprot_blastx_out} | cut -d, -f 1`;
    do if [ "$i" == "$j" ]; 
        then count=$((count+1)); 
        else count=1 ; 
       fi ;
       echo $count ;
       j="$i";
   done
}

INPUTBLASTOUT=$1
OUTPREFIX=$2

grep -ve "^#" ${INPUTBLASTOUT} | tr ":" "\t" | tr "," "\t" | cut -d $'\t' -f 1,2,3,4,5,7 > ${OUTPREFIX}_uniprot_blastx.out.tsv;
colonne_rank_query ${INPUTBLASTOUT} > ${OUTPREFIX}_uniprot_blastx.out.colon_id;
paste ${OUTPREFIX}_uniprot_blastx.out.tsv ${OUTPREFIX}_uniprot_blastx.out.colon_id > ${OUTPREFIX}_uniprot_blastx_no_header.tsv;
cat <(echo -e "chrom\tpos\tuniprot\tEvalue\tbitscore\tidentity\trank") ${OUTPREFIX}_uniprot_blastx_no_header.tsv > ${OUTPREFIX}_uniprot_blastx.tsv;
rm ${OUTPREFIX}_uniprot_blastx.out.tsv ${OUTPREFIX}_uniprot_blastx.out.colon_id ${OUTPREFIX}_uniprot_blastx_no_header.tsv;
