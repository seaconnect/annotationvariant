## anvage
pip3 install anvage

## ncbi blast local
wget https://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/ncbi-blast-2.11.0+-x64-linux.tar.gz
tar zxvpf ncbi-blast-2.11.0+-x64-linux.tar.gz
mv ncbi-* $HOME/src
export PATH=$PATH:/$HOME/src/ncbi-blast-2.10.1+/bin
mkdir $HOME/src/blastdb
export BLASTDB=$HOME/src/blastdb
#perl $HOME/src/ncbi-blast-2.10.1+/bin/update_blastdb.pl --help
## show all ncbi preformated database
#perl ${HOME}/src/ncbi-blast-2.11.0+/bin/update_blastdb.pl --showall
## download swissprot database
#perl ${HOME}/src/ncbi-blast-2.11.0+/bin/update_blastdb.pl --decompress swissprot
#mv * $HOME/src/blastdb
#swissprot
#refseq_protein
cd $HOME/src/blastdb
wget ftp://ftp.ebi.ac.uk/pub/databases/fastafiles/uniprot/uniprotkb_swissprot.gz
zcat uniprotkb_swissprot.gz | awk '{if (/^>/) { print ">" $2} else { print $_}}' > swissprot.fasta
makeblastdb -in swissprot.fasta -dbtype prot -input_type fasta -parse_seqids -out swissprot_prot -title swissprot_prot

## build container
sudo singularity build -s snpsdata_analysis.simg Singularity.snpsdata_analysis

