# annotationVariant

Seek positions of variants onto genome and check if they are on an annotated region e.g.  coding region. Then seek the proteic function of the gene


```
bash locate_coding_snps.sh
```


Get flanking genome DNA sequences centered on variant positions

```
bash flanking_sequences_snps.sh
```

Align flanking sequences against UNIPROT-swissprot sequences database using `blastx` and retrieve UNIPROT id

```
bash uniprot_sequences.sh
```


Keep only nonsynonymous variants

```
bash keep_nonsynonymous.sh
```


_____________________________________________________________


## Revision

Organise folders as follows:

```
.
├── annotation   -> genome annotations
├── diplodus     -> output files for D. sargus
├── filtered     -> filtered SNP input files
├── genomes      -> genomes
├── mullus       -> output files for M. surmuletus
└── raw          -> raw SNP input files
```
