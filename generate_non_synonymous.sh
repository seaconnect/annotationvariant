## Diplodus
#1 vcf input
VCF='diplodus_CDS.vcf'
#2 genome fasta
GENOME_FAS='genomes/diplodus_genome_lgt6000.fasta' 
#3 genome gff3 (annotation)
GFF3='annotation/sar_annotation.gff3'
#4 prefix
PREFIX='diplodus_CDS'

## Mullus
#1 vcf input
VCF='mullus/mullus_CDS.vcf'
#2 genome fasta
GENOME_FAS='genomes/mullus_genome_lgt6000.fasta' 
#3 genome gff3 (annotation)
GFF3='annotation/mullus_annotation.gff3'
#4 prefix
PREFIX='mullus/mullus_CDS'

## run anvage with file specs above

anvage synonymous --vcf ${VCF} \
--genome ${GENOME_FAS} \
--annotation ${GFF3} \
--output_prefix ${PREFIX}