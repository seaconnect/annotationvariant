SINGULARITY_SIMG=snpsdata_analysis.simg
singularity shell $SINGULARITY_SIMG

## select SNPs on an exon|CDS|mRNA|gene

intersect_vcf_annotation () {
  #1 output prefix
  #2 intput vcf
  #3 gff3 genome annotation
  #4 exon|CDS|mRNA|gene

  bedtools intersect -wb -a $2 -b $3 | grep $4 | rev | cut -f10- | rev | uniq > $1_$4.bed
  wait;
  cat <(grep "^#" $2) $1_$4.bed > $1_$4.vcf
}

# diplodus sargus (only on outlier SNPs)
for annot in `echo "exon CDS mRNA gene"`; do intersect_vcf_annotation "diplodus" "raw/dip_adaptive_413.vcf" "annotation/sar_annotation.gff3" ${annot} ; done
# mullus surmuletus (only on outlier SNPs)
for annot in `echo "exon CDS mRNA gene"`; do intersect_vcf_annotation "mullus" "raw/mul_adaptive_291.vcf" "annotation/mullus_annotation.gff3" ${annot} ; done

## Diplodus 
# full: separate all filtered SNPs into gene, mRNA, CDS and exon
for annot in `echo "exon CDS mRNA gene"`; do intersect_vcf_annotation "diplodus/diplodus" "filtered/dip_all_filtered.vcf" "annotation/sar_annotation.gff3" ${annot} ; done
# extract non-gene SNP
# first get list postitions gene SNP
grep -v "^##" "diplodus_gene.vcf" | cut -f1-2 | sed '1d' > dip_gene_pos.txt
## remove gene SNP from total (filtered) vcf
vcftools --vcf "filtered/dip_all_filtered.vcf" --exclude-positions "dip_gene_pos.txt" --recode --recode-INFO-all --out "dip_nongene"

## Mullus
# full: separate all filtered SNPs into gene, mRNA, CDS and exon
for annot in `echo "exon CDS mRNA gene"`; do intersect_vcf_annotation "mullus/mullus" "filtered/mul_all_filtered.vcf" "annotation/mullus_annotation.gff3" ${annot} ; done
# extract non-gene SNP
# first get list postitions gene SNP
grep -v "^##" "mullus/mullus_gene.vcf" | cut -f1-2 | sed '1d' > "mullus/mullus_gene_pos.txt"

## remove gene SNP from total (filtered) vcf
vcftools --vcf "filtered/mul_all_filtered.vcf" --exclude-positions "mullus/mullus_gene_pos.txt" --recode --recode-INFO-all --out "mullus/mullus_nongene"
mv "mullus/mullus_nongene.recode.vcf" "mullus/mullus_nongene.vcf"