## subset snps by type (nongene, gene, mRNA, CDS...)

bash locate_coding_snps.sh

## on CDS (in this instance) subset nonynonymous/synonymous

bash generate_non_synonymous.sh

# count the # of SNPs each generated file
awk '!/#/' mullus/mullus_CDS_nonsynonymous.vcf | wc -l 
awk '!/#/' diplodus/diplodus_CDS_nonsynonymous.vcf | wc -l 

## seek known uniprot reference on the genome flanking sequence of non synonymous SNPs
#### get flanking region of non synonymous SNPs
i="diplodus"
i="mullus"
anvage flank -f ${i}/${i}_CDS_nonsynonymous.vcf -g genomes/${i}_genome_lgt6000.fasta -w 200 -o ${i}/${i}_CDS_nonsynonymous;

#### blast flanking sequences against uniprot
###### diplodus
blastx -db swissprot_nucl -query "diplodus/diplodus_CDS_nonsynonymous_flanking.fasta" -task blastx -outfmt "7 delim=, qacc sacc evalue bitscore qcovus pident" -max_target_seqs 5 > diplodus_CDS_nonsynonymous_uniprot.blastout
bash blastout2tsv.sh diplodus_CDS_nonsynonymous_uniprot.blastout diplodus_CDS_nonsynonymous
###### mullus
blastx -db swissprot_nucl -query "mullus/mullus_CDS_nonsynonymous_flanking.fasta" -task blastx -outfmt "7 delim=, qacc sacc evalue bitscore qcovus pident" -max_target_seqs 5 > mullus_CDS_nonsynonymous_uniprot.blastout
bash blastout2tsv.sh mullus_CDS_nonsynonymous_uniprot.blastout mullus_CDS_nonsynonymous
