colonne_rank_query () {
   uniprot_blastx_out=$1
   j="rien"
   count=0
   for i in `grep -ve "^#" ${uniprot_blastx_out} | cut -d $'\t' -f 1`;
    do if [ "$i" == "$j" ]; 
        then count=$((count+1)); 
        else count=1 ; 
       fi ;
       echo $count ;
       j="$i";
   done
}

## blast flanking sequences against uniprot references
## convert blast output into a nice table
for orgn in `echo diplodus mullus`;
   do for typeanot in `echo CDS` ;     
     do blastx -db swissprot_nucl -query ${orgn}_${typeanot}_flanking.fasta -task blastx -outfmt "7 delim=, qacc sacc evalue bitscore qcovus pident" -max_target_seqs 5 > ${orgn}_${typeanot}_uniprot_blastx.out;
     grep -ve "^#" ${orgn}_${typeanot}_uniprot_blastx.out | tr ":" "\t" | cut -d $'\t' -f 1,2,3,4,5,7 > ${orgn}_${typeanot}_uniprot_blastx.out.tsv;
     colonne_rank_query ${orgn}_${typeanot}_uniprot_blastx.out > ${orgn}_${typeanot}_uniprot_blastx.out.colon_id;
     paste ${orgn}_${typeanot}_uniprot_blastx.out.tsv ${orgn}_${typeanot}_uniprot_blastx.out.colon_id > ${orgn}_${typeanot}_uniprot_blastx_no_header.tsv;
     cat <(echo -e "chrom\tpos\tuniprot\tEvalue\tbitscore\tidentity\trank") ${orgn}_${typeanot}_uniprot_blastx_no_header.tsv > ${orgn}_${typeanot}_uniprot_blastx.tsv;
     rm ${orgn}_${typeanot}_uniprot_blastx.out.tsv ${orgn}_${typeanot}_uniprot_blastx.out.colon_id ${orgn}_${typeanot}_uniprot_blastx_no_header.tsv;
   done
done


