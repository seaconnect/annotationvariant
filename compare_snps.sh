# compare lists of SNP 

## Diplodus
# get CDS positions
grep -v "^##" "diplodus/diplodus_CDS.vcf" | cut -f1-2 | sed '1d' > "diplodus/diplodus_CDS_pos.txt"
# get CDS non-synonymous positions
grep -v "^##" "diplodus/diplodus_CDS_nonsynonymous.vcf" | cut -f1-2 | sed '1d' > "diplodus/diplodus_CDS_nonsynonymous_pos.txt"
# get pcadapt outlier positions
grep -v "^##" "../../seaConnect--dataPrep/04-finalPrep/01-Diplodus/dip_adaptive_413.vcf" | cut -f1-2 | sed '1d' > "diplodus/diplodus_pcadapt_pos.txt"

## Mullus
# get CDS positions
grep -v "^##" "mullus/mullus_CDS.vcf" | cut -f1-2 | sed '1d' > "mullus/mullus_CDS_pos.txt"
# get CDS nonsynonymous positions
grep -v "^##" "mullus/mullus_CDS_nonsynonymous.vcf" | cut -f1-2 | sed '1d' > "mullus/mullus_CDS_nonsynonymous_pos.txt"
# get pcadapt outlier positions
grep -v "^##" "../../seaConnect--dataPrep/04-finalPrep/02-Mullus/mul_adaptive_291.vcf" | cut -f1-2 | sed '1d' > "mullus/mullus_pcadapt_pos.txt"

